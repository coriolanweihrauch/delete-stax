<?php

use yii\db\Migration;

class m180724_173156_create_table_transaction extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'status_id' => $this->integer()->unsigned(),
            'errormessage' => $this->string(),
            'userfrom_id' => $this->integer()->unsigned(),
            'userto_id' => $this->integer(),
            'message' => $this->string(),
            'created' => $this->dateTime(),
            'processed' => $this->dateTime(),
            'currency_id' => $this->integer()->unsigned(),
            'amount' => $this->bigInteger()->notNull(),
            'balancefrom' => $this->bigInteger()->notNull(),
            'balanceto' => $this->bigInteger()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%transaction}}');
    }
}
