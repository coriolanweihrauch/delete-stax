<?php

use yii\db\Migration;

class m180724_173156_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'email' => $this->string()->notNull(),
            'bitcoinwalletid' => $this->string(),
            'bitcoinbalance' => $this->bigInteger()->unsigned(),
            'bitcoinmaxamount' => $this->bigInteger()->unsigned(),
            'ethereumwalletid' => $this->string(),
            'ethereumbalance' => $this->bigInteger()->unsigned(),
            'ethereummaxamount' => $this->bigInteger()->unsigned(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
