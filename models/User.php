<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $email
 * @property string $bitcoinwalletid
 * @property string $bitcoinbalance
 * @property string $bitcoinmaxamount
 * @property string $ethereumwalletid
 * @property string $ethereumbalance
 * @property string $ethereummaxamount
 */
class User extends \yii\db\ActiveRecord
{
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_LOGIN    = 'login';
    const SCENARIO_TOPUP    = 'topup';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function getTransactions()
    {
        return Transaction::find()->where(['or', ['userfrom_id' => $this->id], ['userto_id' => $this->id]])->orderBy(['created' => SORT_DESC])->all();
    }

    /**
     * {@inheritdoc}
     */
    public $currency_id;
    public $amount;
    public function rules()
    {
        return [
            // Field required on registration
            [['name', 'description', 'email'], 'required', 'on' => [self::SCENARIO_REGISTER]],

            // Field required on topup
            [['currency_id', 'amount'], 'required', 'on' => [self::SCENARIO_TOPUP]],


            [['name', 'email'], 'required'],
            [['bitcoinbalance', 'bitcoinmaxamount', 'ethereumbalance', 'ethereummaxamount'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['description', 'email'], 'string', 'max' => 1024],
            [['bitcoinwalletid'], 'string', 'max' => 35],
            [['ethereumwalletid'], 'string', 'max' => 40],
            // require valid email
            ['email', 'email'],
            // validate custom fields
            [['currency_id'], 'integer'],
            [['amount'], 'number', 'min'=>0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'email' => 'Email',
            'bitcoinwalletid' => 'Bitcoin Wallet',
            'bitcoinbalance' => 'Bitcoin Balance',
            'bitcoinmaxamount' => 'Bitcoin Limit',
            'ethereumwalletid' => 'Ethereum Wallet',
            'ethereumbalance' => 'Ethereum Balance',
            'ethereummaxamount' => 'Ethereum Limit',
        ];
    }

    public function __toString()
    {
        // todo: should we use wallet IDs instead ?
//         return $this->name ." <".$this->email.">";
        return (string) $this->id;
    }

    public function topup()
    {
        if( $this->currency_id == Transaction::CURRENCY_BITCOIN)
        {
            $this->bitcoinbalance += Transaction::to_bitcoin_internal_format($this->amount);
            // todo: output save error messages
            $this->save();
            return [
                'currency' => Transaction::NAME_BITCOIN,
                'balance' => Transaction::to_bitcoin_external_format($this->bitcoinbalance)
            ];
        }
        elseif( $this->currency_id == Transaction::CURRENCY_ETHEREUM )
        {
            $this->ethereumbalance += Transaction::to_ethereum_internal_format($this->amount);
            // todo: output save error messages
            $this->save();
            return [
                'currency' => Transaction::NAME_ETHEREUM,
                'balance' => Transaction::to_ethereum_external_format($this->ethereumbalance)
            ];
        }
        else
        {
            return ['error' => 'invalid currency'];
        }
    }

    public function getTransactionList()
    {
        $arrTransactions = array();
        foreach($this->transactions as $transaction)
        {
            $arrTransactions[] = $transaction->summaryArray;
        }
        return $arrTransactions;
    }
}
