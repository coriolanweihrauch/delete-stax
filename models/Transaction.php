<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $status_id
 * @property int $userfrom_id
 * @property int $userto_id
 * @property string $created
 * @property string $processed
 * @property int $currency_id
 * @property int $amount
 * @property int $balancefrom
 * @property int $balanceto
 */
class Transaction extends \yii\db\ActiveRecord
{
    const CURRENCY_BITCOIN      = 1;
    const CURRENCY_ETHEREUM     = 2;

    const MULTIPLIER_BITCOIN    = 100000000;
    const MULTIPLIER_ETHEREUM   = 1000000;

    const NAME_BITCOIN          = "BTC";
    const NAME_ETHEREUM         = "ETH";

    const SCENARIO_TRANSFER     = 'transfer';

    const STATUS_NEW            = 1;
    const STATUS_FAILED         = 2;
    const STATUS_COMPLETED      = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    public function getUserfrom()
    {
        return $this->hasOne(User::className(), ['id' => 'userfrom_id']);
    }

    public function getUserto()
    {
        return $this->hasOne(User::className(), ['id' => 'userto_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // Fields required on transaction
            [['userfrom_id', 'userto_id', 'currency_id', 'amount'], 'required', 'on' => [self::SCENARIO_TRANSFER]],

            [['status_id', 'userfrom_id', 'userto_id', 'currency_id', 'balancefrom', 'balanceto'], 'integer'],
            [['created', 'processed'], 'safe'],
//             [['amount', 'balancefrom', 'balanceto'], 'required'],

            // custom validation
            [['amount'], 'number', 'min'=>0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status',
            'userfrom_id' => 'From',
            'userto_id' => 'To',
            'created' => 'Created on',
            'processed' => 'Processed on',
            'currency_id' => 'Currency',
            'amount' => 'Amount',
            'balancefrom' => 'Balance (From)',
            'balanceto' => 'Balance (To)',
        ];
    }

    public static function to_bitcoin_internal_format($amount)
    {
        return $amount * self::MULTIPLIER_BITCOIN;
    }

    public static function to_bitcoin_external_format($amount)
    {
        return number_format( $amount / self::MULTIPLIER_BITCOIN, 9);
    }

    public static function to_ethereum_internal_format($amount)
    {
        return $amount * self::MULTIPLIER_ETHEREUM;
    }

    public static function to_ethereum_external_format($amount)
    {
        return number_format( $amount / self::MULTIPLIER_ETHEREUM, 6);
    }

    public function transfer()
    {
        if(($userfrom = User::findOne($this->userfrom_id)) == null)
            return ['error' => 'User from does not exist'];
        if(($userto = User::findOne($this->userto_id)) == null)
            return ['error' => 'User to does not exist'];
        if($this->currency_id == self::CURRENCY_BITCOIN)
        {
            $this->amount = $this->to_bitcoin_internal_format($this->amount);
            if($this->amount > $this->to_bitcoin_internal_format($userfrom->bitcoinmaxamount))
                return ['error' => 'Amount exceeds user limit'];
        }
        elseif($this->currency_id == self::CURRENCY_ETHEREUM)
        {
            $this->amount = $this->to_ethereum_internal_format($this->amount);
            if($this->amount > $this->to_ethereum_internal_format($userfrom->ethereummaxamount))
                return ['error' => 'Amount exceeds user limit'];
        }
        else
        {
            return ['error' => 'Invalid currency'];
        }
        $this->save();
        return ['transaction_id' => $this->id];
    }

    public function getStatusName()
    {
        switch($this->status_id)
        {
            case self::STATUS_NEW: return "New"; break;
            case self::STATUS_FAILED: return "Failed"; break;
            case self::STATUS_COMPLETED: return "Completed"; break;
            default: return "Unknown";
        }
    }

    public function getUserfromName()
    {
        return (string) $this->userfrom;
    }

    public function getUsertoName()
    {
        return (string) $this->userto;
    }

    public function getCurrencyName()
    {
        switch($this->status_id)
        {
            case self::CURRENCY_BITCOIN: return self::NAME_BITCOIN; break;
            case self::CURRENCY_ETHEREUM: return self::NAME_ETHEREUM; break;
            default: return "unknown";
        }
    }

    public function getAmountFormatted()
    {
        // todo: add additiional formatting if needed
        return $this->to_ethereum_external_format($this->amount);
    }

    public function getSummaryArray()
    {
        return [
            'id'        => $this->id,
            'status'    => $this->statusName,
            'from'      => $this->userfromName,
            'to'        => $this->usertoName,
            'currency'  => $this->currencyName,
            'amount'    => $this->amountFormatted,
        ];
    }

    public function process()
    {
        // Default
        $this->status_id = self::STATUS_FAILED;
        if(($userfrom = User::findOne($this->userfrom_id)) == null)
        {
            $this->errormessage = 'User from does not exist';
            $this->save(false);
            return;
        }
        if(($userto = User::findOne($this->userto_id)) == null)
        {
            $this->errormessage = 'User to does not exist';
            $this->save(false);
            return;
        }

        if($this->currency_id == self::CURRENCY_BITCOIN)
        {
            if($this->amount > $userfrom->bitcoinmaxamount)
            {
                $this->errormessage = 'Amount exceeds user limit';
                $this->save(false);
                return;
            }
            if($this->amount > $userfrom->bitcoinbalance)
            {
                $this->errormessage = 'Amount exceeds user balance';
                $this->save(false);
                return;
            }
            $userfrom->bitcoinbalance = $userfrom->bitcoinbalance - $this->amount;
            $userfrom->save(false);
            $userto->bitcoinbalance = $userto->bitcoinbalance - $this->amount;
            $userto->save(false);
            $this->balancefrom = $userfrom->bitcoinbalance;
            $this->balanceto = $userto->bitcoinbalance;
            $this->status_id = self::STATUS_COMPLETED;
            $this->save(false);
        }
        elseif($this->currency_id == self::CURRENCY_ETHEREUM)
        {
            if($this->amount > $userfrom->ethereummaxamount)
            {
                $this->errormessage = 'Amount exceeds user limit';
                $this->save(false);
                return;
            }
            $userfrom->ethereumbalance = $userfrom->ethereumbalance - $this->amount;
            $userfrom->save(false);
            $userto->ethereumbalance = $userto->ethereumbalance - $this->amount;
            $userto->save(false);
            $this->balancefrom = $userfrom->ethereumbalance;
            $this->balanceto = $userto->ethereumbalance;
            $this->status_id = self::STATUS_COMPLETED;
            $this->save(false);
        }
        else
        {
            $this->errormessage = 'Invalid currency';
            $this->save(false);
            return;
        }
    }

    public function getSummary()
    {
        return $this->id.": ".$this->statusName.($this->errormessage ? " - error: ".$this->errormessage : "");
    }
}
