REQUIREMENTS
------------

apache, php7, mysql (mariadb)

~~~
aptitude install apache2 php php-mysql mysql
~~~


INSTALLATION
------------

~~~
cd /var/www
mkdir stax && cd stax
git init
git remote add origin git@bitbucket.org:coriolanweihrauch/stax.git
git pull 
php composer update
~~~

Configure apache
~~~
sudo bash -c 'cat <<EOF >> /etc/apache2/sites-available/123-stax.local.conf
<VirtualHost *:80>
        Servername "stax.local"
        DocumentRoot "/var/www/stax/web"
<Directory "/var/www/stax/web">
        allow from all
        Options +Indexes
</Directory>
</VirtualHost>
EOF'
sudo a2ensite 123-stax.local
sudo apachectl graceful
~~~

Add stax.local to your hosts and browse to http://stax.local