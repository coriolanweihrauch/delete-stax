<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bitcoinwalletid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bitcoinbalance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bitcoinmaxamount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ethereumwalletid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ethereumbalance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ethereummaxamount')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
