<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'bitcoinwalletid') ?>

    <?php // echo $form->field($model, 'bitcoinbalance') ?>

    <?php // echo $form->field($model, 'bitcoinmaxamount') ?>

    <?php // echo $form->field($model, 'ethereumwalletid') ?>

    <?php // echo $form->field($model, 'ethereumbalance') ?>

    <?php // echo $form->field($model, 'ethereummaxamount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
