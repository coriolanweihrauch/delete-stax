<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>STAX</h1>

        <div><?= Html::a("Click here to access the API", ['api/index']); ?></div>
        <div>Please see the file "assignment notes.odt" in the documentation folder of the source code for more information</div>

    </div>
</div>
