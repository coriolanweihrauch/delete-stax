<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Transaction;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * APIController - stax public API
 */
class ApiController extends Controller
{
    public function beforeAction($action)
    {
        if (
            $action->id == 'register' ||
            $action->id == 'topup' ||
            $action->id == 'transfer'
            ) {
            $this->enableCsrfValidation = false;
        }
        // skip these actions ...
        if(
            $action->id !='index' &&
            $action->id !='login' &&
            $action->id !='logout' &&
            $action->id !='register'
        )
        {
            // for all others, verify access:
            $this->verifyAccess();
        }
        return parent::beforeAction($action);
    }


    /**
     * Default index
     *
     * Example:
     * curl stax.miastudio.in/api/index
     * Welcome to the STAX API. Additional documentation available on https://stax...
     */
    public function actionIndex()
    {
        echo "Welcome to the STAX API. Additional documentation available on https://stax...";
    }

    /**
     * Fake validation method to verify token
     */
    const TOKEN = "jTLKxkLKExosoKibfroxHQbNJysNLqMQ";
    private function verifyAccess()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(Yii::$app->session['token'] != self::TOKEN)
            throw new \yii\web\HttpException(401, 'Invalid token');
    }

    /**
     * Fake login method to set token
     *
     * Example:
     * curl stax.miastudio.in/api/login -c /tmp/cookiejar
     * Logged in successfully
     */
    public function actionLogin()
    {
        Yii::$app->session->set('token',self::TOKEN);
        echo "Logged in successfully";
    }

    /**
     * Fake login method
     *
     * Example:
     * curl stax.local/api/login
     * Logged in successfully
     */
    public function actionLogout()
    {
        Yii::$app->session->destroy();
        echo "Logged out";
    }

    /**
     * Create basic user
     *
     * Example:
     * curl -d "User[name]=john+doe&User[description]=my+bio&User[email]=johndoe@example.com" -X POST http://stax.miastudio.in/api/register
     * {"user_id":8}
     *
     */
    public function actionRegister()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new User();
        $model->scenario = User::SCENARIO_REGISTER;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['user_id' => $model->id];
        }
        else
            return ['error' => $model->getErrors()];
    }

    /**
     * Topup currency for user
     *
     * Example
     * curl -d "User[amount]=0.001&User[currency_id]=1" -X POST stax.miastudio.in/api/topup?id=1 -L -b /tmp/cookiejar
     * {"currency":"BTC","balance":"0.004000000"}
     *
     * @param int @id user id
     */
    public function actionTopup($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (($model = User::findOne($id)) === null) {
            throw new NotFoundHttpException('User does not exist');
        }

        $model->scenario = User::SCENARIO_TOPUP;
        $model->load(Yii::$app->request->post());

        if ($model->validate()) {
            return $model->topup();
        }
        else
            return ['error' => $model->getErrors()];
    }

    /**
     * Transfer funds
     *
     * Example:
     * curl -d "Transaction[userfrom_id]=1&Transaction[userto_id]=1&Transaction[currency_id]=1&Transaction[amount]=0.123" -X POST stax.miastudio.in/api/transfer -L -b /tmp/cookiejar
     * {"transaction_id":2}
     */
    public function actionTransfer()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new Transaction();
        $model->load(Yii::$app->request->post());
        $model->status_id = Transaction::STATUS_NEW;
        $model->created = date("Y:m:d H:i:s");
        $model->scenario = Transaction::SCENARIO_TRANSFER;

        if ($model->validate()) {
            return $model->transfer();
        }
        else
            return ['error' => $model->getErrors()];
    }

    /**
     * List transactions
     *
     * Example:
     * curl stax.miastudio.in/api/transactions?id=1 -L -b /tmp/cookiejar
     * [{"id":2,"status":"Completed","from":"2","to":"1","currency":"unknown","amount":"0.000002"},{"id":1,"status":"Completed","from":"1","to":"2","currency":"unknown","amount":"0.000001"}]
     */
    public function actionTransactions($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (($model = User::findOne($id)) === null) {
            throw new NotFoundHttpException('User does not exist');
        }

        return $model->transactionList;
    }

    /**
     * Retreieve transaction status
     *
     * Example:
     * curl stax.miastudio.in/api/transactionstatus?id=1 -L -b /tmp/cookiejar
     * {"status":"New"}
     */
    public function actionTransactionstatus($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (($model = Transaction::findOne($id)) === null) {
            throw new NotFoundHttpException('Transaction does not exist');
        }

        return ['status' => $model->statusName];
    }
}