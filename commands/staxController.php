<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\User;
use app\models\Transaction;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StaxController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        echo "Welcome to STAX console\n";

        return ExitCode::OK;
    }

    /**
     * Process all pending transactions in queue
     *
     * Example to run manually:
     * $ php yii stax/queue
     *   2: Completed
     *   1: Completed
     *
     *
     * This should be run by a cron job such as
     * $ sudo crontab -e -u www-data
     * add:
     * *\/1 * * * * php /var/www/stax/yii stax/queue >> /var/log/stax/queue.log 2>&1
     *
     */
    public function actionQueue()
    {
        $limit = 10; // todo: set this in configuration
        // todo: add optimistic locking here, if needed
        // https://www.yiiframework.com/doc/guide/2.0/en/db-active-record#optimistic-locks
        $models = Transaction::find()->where(['status_id' => Transaction::STATUS_NEW])->orderBy(['created' => SORT_DESC])->limit($limit)->all();
        foreach($models as $transaction)
        {
            $transaction->process();
            echo $transaction->summary."\n";
        }
        return ExitCode::OK;
    }
}
